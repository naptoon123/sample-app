package com.admin.lab;

interface InngageConstants {

    String inngageAppToken = "68e5c0d9d73fc9cc1e1d28b741a4394d";
    String inngageEnvironment = "dev";
    String googleMessageProvider = "FCM";

    int updateInterval = 60000;
    int priorityAccuracy = 104;
    int displacement = 100;
    int PERMISSION_ACCESS_LOCATION_CODE = 99;
    String LOCATION_NOT_FOUND = "Não foi possível obter a localização do usuário";
}