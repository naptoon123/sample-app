package com.admin.lab;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import br.com.inngage.sdk.GrantPermission;
import br.com.inngage.sdk.InngageIntentService;
import br.com.inngage.sdk.InngagePermissionUtil;
import br.com.inngage.sdk.InngageServiceUtils;
import br.com.inngage.sdk.InngageUtils;
import br.com.inngage.sdk.InngageWebViewActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TAG" ;
    InngageServiceUtils serviceUtils = new InngageServiceUtils(MainActivity.this);
    public InngagePermissionUtil.PermissionRequestObject mBothPermissionRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this,"App Started",Toast.LENGTH_LONG).show();
        handleSubscription();

        handleNotification();

        handleLocation();
    }


    private void handleSubscription() {

        //JSONObject jsonCustomField = new JSONObject();

//        try {
//
////            jsonCustomField.put("nome", "Mohamed Ali");
////            jsonCustomField.put("email", "naptoon.ahmed@gmail.com");
////            jsonCustomField.put("telefone", "+21652000562");
////            jsonCustomField.put("dataRegistro", "2018-10-16");
////            jsonCustomField.put("dataNascimento", "1996-11-19");
//
//
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }

        InngageIntentService.startInit(
                this,
                InngageConstants.inngageAppToken,
                "medalinakouri@gmail.com",
                InngageConstants.inngageEnvironment,
                InngageConstants.googleMessageProvider);
    }
    private void handleNotification() {

        String notifyID = "", title = "", body = "", url = "";

        Bundle bundle = getIntent().getExtras();

        if (getIntent().hasExtra("EXTRA_NOTIFICATION_ID")) {

            notifyID = bundle.getString("EXTRA_NOTIFICATION_ID");
        }
        if (getIntent().hasExtra("EXTRA_TITLE")) {

            title = bundle.getString("EXTRA_TITLE");
        }
        if (getIntent().hasExtra("EXTRA_BODY")) {

            body = bundle.getString("EXTRA_BODY");
        }

        if (getIntent().hasExtra("EXTRA_URL")) {

            url = bundle.getString("EXTRA_URL");
        }


        if (url.isEmpty()) {
            if (!"".equals(notifyID) || !"".equals(title) || !"".equals(body)) {
                Log.d(TAG, "no link: " + url);
                InngageUtils.showDialog(
                        title,
                        body,
                        notifyID,
                        InngageConstants.inngageAppToken,
                        InngageConstants.inngageEnvironment,
                        this);


            }

        } else if (!"".equals(notifyID) || !"".equals(title) || !"".equals(body)) {
            Log.d(TAG, "Link: " + url);
            InngageUtils.showDialogwithLink(
                    title,
                    body,
                    notifyID,
                    InngageConstants.inngageAppToken,
                    InngageConstants.inngageEnvironment,
                    url,
                    this);

        }

    }
    public void askPermissions() {
        mBothPermissionRequest =
                InngagePermissionUtil.with(this).request(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION).onResult(
                        new GrantPermission() {
                            @Override
                            protected void call(int requestCode, String[] permissions, int[] grantResults) {

                                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                                    serviceUtils.startService(
                                            InngageConstants.updateInterval,
                                            InngageConstants.priorityAccuracy,
                                            InngageConstants.displacement,
                                            InngageConstants.inngageAppToken);

                                } else {

                                    Toast.makeText(MainActivity.this, InngageConstants.LOCATION_NOT_FOUND, Toast.LENGTH_LONG).show();
                                }
                            }

                        }).ask(InngageConstants.PERMISSION_ACCESS_LOCATION_CODE);
    }
    private void handleLocation() {

        if (InngageUtils.hasM() && !(ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

            askPermissions();

        } else {

            serviceUtils.startService(
                    InngageConstants.updateInterval,
                    InngageConstants.priorityAccuracy,
                    InngageConstants.displacement,
                    InngageConstants.inngageAppToken);
        }
    }

    
}
